#!/usr/bin/make -f
#
# Prerequisites: apt install ruby-kramdown-rfc2629 xml2rfc
#

draft = abuse-resistant-keystore
OUTPUT = $(draft).txt $(draft).html $(draft).xml

all: $(OUTPUT)

%.xml: %.md
	kramdown-rfc --v3 < $< > $@.tmp
	mv $@.tmp $@

%.html: %.xml
	xml2rfc $< --html

%.txt: %.xml
	xml2rfc $< --text

clean:
	-rm -rf $(OUTPUT)

check:
	codespell $(draft).md

.PHONY: clean all check
